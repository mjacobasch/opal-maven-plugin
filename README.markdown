# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Adding the OPAL BugPicker maven plugin ###

Add this snippet to your pom.xml.

```
<plugin>
  <groupId>de.opal-project</groupId>
  <artifactId>opal-bugpicker-maven-plugin</artifactId>
  <version>0.0.3-SNAPSHOT</version>
  <executions>
    <execution>
      <goals>
        <goal>analyse</goal>
      </goals>
    </execution>
  </executions>
</plugin>
```

This is an example containing every possible value available as configuration.

```
<plugin>
  <groupId>de.opal-project</groupId>
  <artifactId>opal-bugpicker-maven-plugin</artifactId>
  <version>0.0.3-SNAPSHOT</version>
  <executions>
    <execution>
      <goals>
        <goal>analyse</goal>
      </goals>
    </execution>
  </executions>
  <configuration>
    <outputDirectory>opal-reports</outputDirectory>
    <reportFileName>report</reportFileName>
    <reportTimestamp>true</reportTimestamp>
    <maxEvalFactor>1.75d</maxEvalFactor>
    <maxEvalTime>10000</maxEvalTime>
    <maxCardinalityOfIntegerRanges>16</maxCardinalityOfIntegerRanges>
  </configuration>
</plugin>
```
