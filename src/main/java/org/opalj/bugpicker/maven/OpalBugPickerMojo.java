/* BSD 2-Clause License:
 * Copyright (c) 2015, Marco Jacobasch, All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  - Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.opalj.bugpicker.maven;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.opalj.br.analyses.ProgressManagement;
import org.opalj.br.analyses.Project;
import org.opalj.bugpicker.core.analysis.AnalysisException;
import org.opalj.bugpicker.core.analysis.BugPickerAnalysis;
import org.opalj.bugpicker.core.analysis.Issue;

import scala.Function1;
import scala.Tuple3;
import scala.collection.Iterable;
import scala.collection.JavaConversions;
import scala.collection.mutable.Buffer;
import scala.runtime.AbstractFunction1;
import scala.xml.Node;

/**
 * 
 */
@Mojo(name = "analyse", defaultPhase = LifecyclePhase.VERIFY, requiresDependencyResolution = ResolutionScope.COMPILE_PLUS_RUNTIME)
public class OpalBugPickerMojo extends AbstractMojo {

    /**
     * The build directory of the project.
     * 
     * Contains the compiled bytecode, used to store the reports, etc.
     */
    @Parameter(defaultValue = "${project.build.directory}", readonly = true, required = true)
    private File buildDirectory;

    /**
     * The project, used to obtain all dependencies/artifacts.
     */
    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    private MavenProject project;

    /**
     * The output directory for the generated reports.
     * 
     * buildDirectory will be used as base path.
     */
    @Parameter(defaultValue = "opal-reports")
    private String outputDirectory;

    /**
     * The filename used to generate the report file names.
     */
    @Parameter(defaultValue = "report")
    private String reportFileName;

    /**
     * Should a timestamp be appended to the report.
     */
    @Parameter(defaultValue = "true")
    private boolean reportTimestamp;

    /**
     * Format used for report timestamp.
     */
    private final static DateFormat DATE_FORMAT = new SimpleDateFormat(
            "yyyy-MM-dd-HH-mm-ss");

    // OPAL analysis settings
    @Parameter(defaultValue = "1.75d")
    private double maxEvalFactor;
    private static final String maxEvalFactorPrefix = "-maxEvalFactor=";

    @Parameter(defaultValue = "10000")
    private int maxEvalTime;
    private static final String maxEvalTimePrefix = "-maxEvalTime=";

    @Parameter(defaultValue = "16")
    private int maxCardinalityOfIntegerRanges;
    private static final String maxCardinalityOfIntegerRangesPrefix = " -maxCardinalityOfIntegerRanges=";

    public void execute() throws MojoExecutionException {

        getLog().info(Messages.SETUP_ANALYSIS);

        final File projectClassFiles = this.getProjectClassFiles();
        final Buffer<File> libraryClassFiles = this.getDependencyArtifacts();
        final Buffer<String> analysisSettings = this.createSettings();
        final File outputFile = this.getOutputFile();

        getLog().info(Messages.SETUP_OPAL_PROJECT);

        final Project<URL> opalProject = this.createProject(projectClassFiles,
                libraryClassFiles);

        getLog().info(Messages.PERFORM_ANALYSIS);

        final Tuple3<Object, Iterable<Issue>, Iterable<AnalysisException>> analysisResult = createBugPickerAnalysis(
                opalProject, analysisSettings);

        getLog().info(Messages.WRITING_REPORT);

        this.writeResult(analysisResult, outputFile);

    }

    /**
     * Creates a {@link File} pointing to the location of all files to be analyzed.
     * 
     */
    private File getProjectClassFiles() {
        return new File(project.getBuild().getOutputDirectory());
    }

    /**
     * Returns a list of all {@link Artifact}s, which are reported by maven as
     * dependencies.
     * 
     * @return a scala {@link Buffer} containing the dependencies.
     */
    private Buffer<File> getDependencyArtifacts() {
        final List<File> dependencies = new ArrayList<File>();

        @SuppressWarnings("unchecked")
        final Set<Artifact> artifacts = project.getArtifacts();
        for (Artifact artifact : artifacts) {
            dependencies.add(artifact.getFile());
        }

        return JavaConversions.asScalaBuffer(dependencies);
    }

    /**
     * Creates a new OPAL project to analyze.
     * 
     * TODO: handle libraries, currently no easy way to construct it in java.
     * 
     * @param projectClassesPath
     *            the path to the files being analyzed.
     * @param dependencyPaths
     *            the paths to the library/dependencies.
     * @return a newly created {@link Project}
     */
    private Project<URL> createProject(File projectClassesPath,
            Buffer<File> dependencyPaths) {
        final Project<URL> opalProject = Project.apply(projectClassesPath);
        return opalProject;
    }

    /**
     * Creates the settings for the opal analysis based on the values defined in the
     * pom.xml or their default values.
     * 
     * @return a {@link Buffer} of the opal analysis settings.
     */
    private Buffer<String> createSettings() {
        final List<String> settings = Arrays.asList(maxEvalFactorPrefix + maxEvalFactor,
                maxEvalTimePrefix + maxEvalTime, maxCardinalityOfIntegerRangesPrefix
                        + maxCardinalityOfIntegerRanges);
        return JavaConversions.asScalaBuffer(settings);
    }

    /**
     * A helper function to create a progress management
     * 
     * @return a {@link Function1} producing a new {@link MavenProgressManagement}
     */
    private Function1<Object, ProgressManagement> createProgressmanagement() {
        return new AbstractFunction1<Object, ProgressManagement>() {

            public ProgressManagement apply(Object arg0) {
                return new MavenProgressManagement(getLog());
            }
        };
    }

    /**
     * Performs a new {@link BugPickerAnalysis} of the given {@link Project} using the
     * given settings.
     * 
     * @param project
     *            the {@link Project} to be analyzed.
     * @param settings
     *            the analysis settings
     * @return
     */
    private Tuple3<Object, Iterable<Issue>, Iterable<AnalysisException>> createBugPickerAnalysis(
            Project<URL> project, Buffer<String> settings) {
        final BugPickerAnalysis analysis = new BugPickerAnalysis();
        return analysis.analyze(project, settings, createProgressmanagement());
    }

    /**
     * Writes the given result as html output to the given file.
     * 
     * @param result
     *            the result of the {@link BugPickerAnalysis}
     * @throws MojoExecutionException
     */
    private void writeResult(
            Tuple3<Object, Iterable<Issue>, Iterable<AnalysisException>> result,
            File outputFile) throws MojoExecutionException {
        final Node xhtmlResult = BugPickerAnalysis.resultsAsXHTML(result._2());

        FileWriter w = null;

        try {
            outputFile = getOutputFile();
            w = new FileWriter(outputFile);
            w.write(xhtmlResult.toString());
        } catch (IOException e) {
            throw new MojoExecutionException("Error creating file " + outputFile, e);
        } finally {
            if (w != null) {
                try {
                    w.close();
                } catch (IOException e) {
                    // ignore
                }
            }
        }
    }

    /**
     * Creates a new {@link File} to output the result using the defined filename and an
     * optional timestamp.
     * 
     * @return a {@link File} to output the result
     */
    private File getOutputFile() {
        final File f = new File(buildDirectory, outputDirectory);

        if (!f.exists()) {
            f.mkdirs();
        }

        File file = null;
        if (reportTimestamp) {
            file = new File(f, reportFileName + "-" + DATE_FORMAT.format(new Date())
                    + ".html");
        } else {
            new File(f, reportFileName + ".html");
        }

        return file;
    }
}
